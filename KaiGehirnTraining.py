import tkttk as tk
import signal
import random
import math
import time
import os

class KaiGehirnTraining(object):
    def __init__(self):
        self.leftPoint = None
        self.rightPoint = None
        self.configFileName = "config.txt"
        self.loadConfiguration()

        self.initGUI()

        self.initCanvas()
        self.initCenterPoint()
        self.setPoints()
        self.root.after(0, self.waitBlink)

        self.initGUIFinish()

    def loadConfiguration(self):
        self.config = dict()
        separators = [":","="]
        saveConfig = False
        if os.path.isfile(self.configFileName):
            with open(self.configFileName, "r") as f:
                configLines = [l.strip(" \n\r") for l in  f.readlines()]
                for l in configLines:
                    for sep in separators:
                        if sep in l:
                            key,value = l.split(sep)
                            self.config[key.strip(" ")] = value.strip(" ")
        else:
            saveConfig = True

        self.config["intervalMin"] = int(self.config.get("intervalMin", 1))
        self.config["intervalMax"] = int(self.config.get("intervalMax", 5))
        self.config["backgroundColor"] = self.config.get("backgroundColor", "#F0F0F0")
        self.config["centerColor"] = self.config.get("centerColor", "blue")
        self.config["pointColor"] = self.config.get("pointColor", "green")
        self.config["blinkColor"] = self.config.get("blinkColor", "red")
        self.config["blinkTime"] = float(self.config.get("blinkTime", 0.5))

        self.waitInterval = []
        self.waitInterval.append(self.config["intervalMin"])
        self.waitInterval.append(self.config["intervalMax"])
        self.backgroundColor = self.config["backgroundColor"]
        self.defaultColorCenterPoint = self.config["centerColor"]
        self.defaultColorPoint = self.config["pointColor"]
        self.blinkColorPoint = self.config["blinkColor"]
        self.blinkTime = self.config["blinkTime"]

        if saveConfig:
            self.saveConfiguration()

    def saveConfiguration(self):
        with open(self.configFileName, "w") as f:
            for k in sorted(self.config.keys()):
                v = self.config[k]
                f.write("{}: {}\n".format(k,v))

    def initGUI(self):
        self.root = tk.Tk()
        self.root.config(background=self.backgroundColor)
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)

        self.root.wm_title("Gehirntraining für Kai")
        self.root.attributes("-fullscreen", True)
        self.root.bind("<Escape>", lambda e: self.close())
        self.root.bind("<space>", lambda e: self.setPoints())

        self.root.update()

    def initGUIFinish(self):
        self.root.mainloop()
        self.close()

    def initCanvas(self):
        self.canvas = tk.Canvas(self.root, bg=self.backgroundColor)
        self.canvas.grid(row=0, column=0, sticky="news")

    def initCenterPoint(self,radius=20):
        self.mX = self.root.winfo_width()/2
        self.mY = self.root.winfo_height()/2
        self.canvas.create_oval(self.mX-radius, self.mY-radius, self.mX+radius, self.mY+radius, fill=self.defaultColorCenterPoint)

    def setPoints(self, radius=20):
        if self.leftPoint:
            self.canvas.delete(self.leftPoint)
        if self.rightPoint:
            self.canvas.delete(self.rightPoint)

        plX = random.randint(radius, self.mX-radius)
        plY = random.randint(radius, self.root.winfo_height()-radius)
        prX = random.randint(self.mX+radius, self.root.winfo_width()-radius)
        prY = random.randint(radius, self.root.winfo_height()-radius)
        while math.hypot(plX-prX,plY-prY) <= self.root.winfo_height()/4:
            prX = random.randint(self.mX+radius, self.root.winfo_width()-radius)
            prY = random.randint(radius, self.root.winfo_height()-radius)

        self.leftPoint = self.canvas.create_oval(plX-radius, plY-radius, plX+radius, plY+radius, fill=self.defaultColorPoint)
        self.rightPoint = self.canvas.create_oval(prX-radius, prY-radius, prX+radius, prY+radius, fill=self.defaultColorPoint)

    def waitBlink(self):
        waitTime = random.randint(*self.waitInterval)
        self.randomBlink()
        self.root.after(int(waitTime*1000), self.waitBlink)

    def randomBlink(self):
        if random.randint(0,1) == 0:
            point = self.leftPoint
        else:
            point = self.rightPoint

        self.canvas.itemconfigure(point, fill=self.blinkColorPoint)
        self.canvas.update()
        time.sleep(self.blinkTime)
        self.canvas.itemconfigure(point, fill=self.defaultColorPoint)
        self.canvas.update()

    def close(self):
        try:
            self.root.destroy()
        except:
            pass

#class SigintHandler(object):
#    def __init__(self):
#        self.callbacks = []
#
#    def appendCallback(self, callbackFunction):
#        self.callbacks.append(callbackFunction)
#
#    def removeCallback(self, callbackFunction):
#        self.callbacks.remove(callbackFunction)
#
#    def callback(self, sig, frame):
#        for f in self.callbacks:
#            f()

if __name__ == "__main__":

#    sigintHandler = SigintHandler()
#    signal.signal(signal.SIGINT, sigintHandler.callback)

    window = KaiGehirnTraining()
